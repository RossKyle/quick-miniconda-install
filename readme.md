# Quick Miniconda Install

_Author: Kyle Ross_

This script installs and updates miniconda (minimal conda). It also adds the default directory that contains packages installed with conda to the system path.

## Collab Notebook version

https://colab.research.google.com/drive/1H9Ver7-tpAvhUvlVLsYYfMQzvNdEAL31?usp=sharingp

## References

1. https://towardsdatascience.com/conda-google-colab-75f7c867a522
